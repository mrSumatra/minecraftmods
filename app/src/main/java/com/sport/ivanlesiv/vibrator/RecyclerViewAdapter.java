package com.sport.ivanlesiv.vibrator;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContextWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>  {

//    RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>
    int count, counter;

    Fragment mapsFragment = new ContentFragment();
    public RecyclerViewAdapter(int count) {
       this.count = count;
       counter= 0;

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutID = R.layout.item_content;
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layoutID, viewGroup,false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(v);

        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int i) {

        itemViewHolder.textView1.setText("item " + i);
        i++;
        itemViewHolder.textView2.setText("item " + i);
        Log.d("qwert","create item");

        itemViewHolder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) ((ContextWrapper)v.getContext())
                        .getBaseContext())
                        .getSupportFragmentManager()
                        .beginTransaction().add(R.id.fragment_menu,mapsFragment).commit();


            }
        });



    }

    @Override
    public int getItemCount() {
        return count;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView1,imageView2;
        TextView textView1,textView2;
        CardView cardView1,cardView2;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView1 = itemView.findViewById(R.id.cardView1);
            cardView2 = itemView.findViewById(R.id.cardView2);

            imageView1 = itemView.findViewById(R.id.imageView1);
            imageView2 = itemView.findViewById(R.id.imageView1);

            textView1 = itemView.findViewById(R.id.textView1);
            textView2 = itemView.findViewById(R.id.textView2);

        }
    }



}
