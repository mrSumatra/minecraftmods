package com.sport.ivanlesiv.vibrator;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import static com.sport.ivanlesiv.vibrator.MainActivity.mainMenuFragment;

public class MeinMenuFragment extends Fragment {

    View view;
    Fragment  modsFragment = new ModsFragment(),
              mapsFragment = new ContentFragment(),
              skinsFragment= new SkinsFragment();

    private void openFragment(Fragment fragment){
        getFragmentManager().beginTransaction().hide(mainMenuFragment).add(R.id.fragment_menu, fragment).commit();
    }

    private View.OnClickListener listener = new View.OnClickListener(){


        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imageButtonMods : openFragment(modsFragment); break;
                case R.id.imageButtonMaps : openFragment(mapsFragment); break;
                case R.id.imageButtonSkins : openFragment(skinsFragment); break;

            }
        }
    };


    //mein menu
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.main_menu, container, false);

        view.findViewById(R.id.imageButtonMods).setOnClickListener(listener);
        view.findViewById(R.id.imageButtonMaps).setOnClickListener(listener);
        view.findViewById(R.id.imageButtonSkins).setOnClickListener(listener);




        return view;

    }
}
