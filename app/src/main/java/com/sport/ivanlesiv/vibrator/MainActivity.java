package com.sport.ivanlesiv.vibrator;


import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;


public class MainActivity extends AppCompatActivity {


    public static MeinMenuFragment mainMenuFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainMenuFragment = new MeinMenuFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_menu,mainMenuFragment, "main")
                .addToBackStack("main")
                .commit();


    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK)
//            getSupportFragmentManager().beginTransaction()
//                    .show(R.id.fragment_menu,mainMenuFragment, "main")
//                    .commit();
//        return super.onKeyDown(keyCode, event);
//    }
}



